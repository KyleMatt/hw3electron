import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomersComponent } from '../customers/customers.component';
import { EmployeeTypeComponent } from '../employee-type/employee-type.component';
import { PoductTypeComponent } from '../poduct-type/poduct-type.component';
import { PoductsComponent } from '../poducts/poducts.component';
import { SaleEmployeeComponent } from '../sale-employee/sale-employee.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatCardModule} from '@angular/material/card';
import { HttpClientModule }    from '@angular/common/http';
import {FormsModule} from '@angular/forms'
import { NgxElectronModule } from 'ngx-electron';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomersComponent,
    EmployeeTypeComponent,
    PoductTypeComponent,
    PoductsComponent,
    SaleEmployeeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatToolbarModule,
    MatMenuModule,
    MatCardModule,
    HttpClientModule,
    FormsModule,
    NgxElectronModule
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
